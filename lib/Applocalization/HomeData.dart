import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/Applocalization/TestNewScreen.dart';
import 'package:untitled/LoginVi/LoginView.dart';
import 'package:untitled/InsideFunctionCalling/CallFunInsideFun.dart';
import 'AppTranslation.dart';
class HomeLangPage extends StatefulWidget {
  @override
  _HomeLangPageState createState() => _HomeLangPageState();
}

class _HomeLangPageState extends State<HomeLangPage>
{

  // String _selectedLang = LocalizationService.langs.first;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text('hello'.tr),
            SizedBox(height: 20),
         /* DropdownButton(
              icon: Icon(Icons.arrow_drop_down),
              value: _selectedLang,
              items: LocalizationService.langs.map((String lang) {
                return DropdownMenuItem(value: lang, child: Text(lang));
              }).toList(),
              onChanged: (String value) {

                // updates dropdown selected value
                print(value);
                setState(() => _selectedLang = value);
                // gets language and changes the locale
              },
            ),*/
            RaisedButton(onPressed: (){
              Get.to(LoginView());
            },
            child: Text("hello".tr),),
            SizedBox(height: 100,),
            RaisedButton(onPressed: (){
              Get.to(MyFunClass());
            },
            child: Text("FunInsideFun".tr),),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    LocalizationService().changeLocale("Türkçe");

    getSingleSubscriptionStream();
  //  getBroadcastStream();
  }


  void getSingleSubscriptionStream() {
    final myStream = NumberCreator()._controller.stream;

    final subscription = myStream.listen(
          (data) => print('Data: $data'),
    );
    /* final subscription1 = myStream.listen(
          (data) => print('Data again: $data'),
    );*/
  }

  void getBroadcastStream() {
    final myStream = NumberCreator()._controller.stream.asBroadcastStream();
    final stream2=NumberCreator()._controller.stream.asBroadcastStream();
    stream2.listen((event) {print("data : $event");});
    final subscription = myStream.listen(
          (data) => print('Data: $data'),
    );

  }

}
class NumberCreator {
  NumberCreator() {
    Timer.periodic(Duration(seconds: 1), (timer) {

      if(_count<10)
        {
          _controller.sink.add(_count++);
        }
    });

  }
  var _count = 1;
  final _controller=new StreamController<int>();
/*

  Stream<int> get stream => _controller.stream
  */
}