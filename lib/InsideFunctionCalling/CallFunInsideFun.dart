import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'Globalfile.dart';
import 'Mycontroller.dart';
class MyFunClass extends StatefulWidget
{
  @override
  State<StatefulWidget> createState()
  {
    return MyView();
  }
}

class MyView extends State<MyFunClass>
{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
  return new Scaffold(
     body: new Column(
       crossAxisAlignment: CrossAxisAlignment.center,
       children: [
         SizedBox(height: 100,),
         GetX<MyGetController>(
           init: MyGetController(),
           builder: (_val)
           {
             return Text(_val.model.value.name.toString());
           },
         ),
         Center(
           child: RaisedButton(

             onPressed: ()
             {
               Get.find<MyGetController>().updatevalue("archana", 21);
               GlobalFileData.funInsideFun(context,getData);
               //Global
             },
           ),
         )
       ],
     ),
   );
  }

  getData(int i)
  {
    print("calla my fun $i");
  }
}