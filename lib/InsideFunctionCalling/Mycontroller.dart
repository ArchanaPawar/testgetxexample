import 'package:get/get.dart';
import 'package:untitled/InsideFunctionCalling/MyModelClass.dart';
class MyGetController extends GetxController
{
  final model=new MyModel().obs;
  updatevalue(String s1, int age1)
  {
    model.update((val) {
      val.name=s1;
      val.age=age1;
    });
  }

}