import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class GlobalFileData
{
  static void funInsideFun(BuildContext context, getData) {
    Get.defaultDialog(
      content: new Container(
        child: new Row(
          children: [
            Expanded(child: RaisedButton(
              child: Text("Yes"),
              onPressed: (){
                Get.back();
                getData(1);
              },
            ),),
            Expanded(child: RaisedButton(
              child: Text("No"),
              onPressed: (){
                Get.back();
              },
            ),),
          ],
        ),
      )
    );
  }
  
}