import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled/PaginationDir/url.dart';
import 'Request.dart';
import 'UserListModel.dart';

class HomeController extends GetxController
{
  var userListData = List<Data>().obs;
  var count=0;
  @override
  void onInit()
  {
    apiGetUserList("1");
    super.onInit();
  }

  void apiGetUserList(String page) async {
    Future.delayed(
        Duration.zero, () => Get.dialog(Center(child: CircularProgressIndicator()),
            barrierDismissible: false));

    Request request = Request(url: urlUserList+page, body: null);
    request.get().then((value) {
      UserListModel userListModel = UserListModel.fromJson(json.decode(value.body));
      userListData.addAll(userListModel.data);
      count=userListModel.totalPages;
      Get.back();
    }).catchError((onError)
    {
      print(onError);
    });
  }
  void deleteItem(int index)
  {
    userListData.removeAt(index);
  }
}