import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled/LoginVi/LoginController.dart';
import 'package:untitled/Applocalization/HomeData.dart';
import 'HomeController.dart';
class MyMaterialApp extends StatefulWidget
{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }

}
class getViewd extends State<MyMaterialApp>
{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return HomeView();
  }

}

class NumberCreateor
{
  int _count =0;
  final _streamcontroler=new StreamController<int>();
  NumberCreator()
  {
    Timer.periodic(Duration(seconds: 1), (timer) { });
    _streamcontroler.sink.add(++_count);
  }
}
class HomeView extends StatelessWidget {
  int page=1;

 // final LoginController _loginController = Get.find();
  final HomeController _homeController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    getStreamfun();
    //print(_loginController.emailTextController.text);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: new Container(
        height: 50,
        alignment: Alignment.center,
        child: /*new RaisedButton(onPressed: ()
        {
           page=page+1;
          _homeController.apiGetUserList((page).toString());
        },
        child: Text("Next"),
        ),*/

          ListView.builder(
            padding: EdgeInsets.only(left: 20.0),
              scrollDirection: Axis.horizontal,
              itemCount:2,
              itemBuilder: (_ctx,index)
              {
                return InkWell(
                onTap: ()
                {
                  _homeController.apiGetUserList((index+1).toString());
                },
              child: Container(
                margin: EdgeInsets.all(5.0),
                width: 40,
                child: new Row(
                  children: [
                    Text((index+1).toString()),
                    SizedBox(width: 2.0,),
                    Container(width: 2.0,height: 40,color: Colors.grey,margin: EdgeInsets.all(10.0),)
                  ],
                ),
              ),
            );
          })
        ),
        appBar: AppBar(
          title: InkWell(
            onTap: (){
              HomeController();
            },
            child: Text('Dashboard',),
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: <Widget>[
              Text(
                'Welcome',
                style: GoogleFonts.exo2(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Expanded(
                child: Obx(
                      () => ListView.builder(
                    itemCount: _homeController.userListData.length,
                    itemBuilder: (context, index) => ListTile(
                      title: Text(
                        _homeController.userListData[index].firstName,
                      ),
                      subtitle: Text(
                        _homeController.userListData[index].email,
                      ),
                      trailing: Image.network(
                        _homeController.userListData[index].avatar,
                        width: 80,
                        height: 80,
                        fit: BoxFit.contain,
                      ),
                      leading: IconButton(
                          icon: Icon(
                            Icons.delete,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            _homeController.deleteItem(index);
                          }),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getStreamfun() {
    final mystream =NumberCreateor()._streamcontroler.stream;
    mystream.listen((event) {print(event);});
  }
}